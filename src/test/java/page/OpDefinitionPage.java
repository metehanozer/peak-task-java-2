package page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import util.PageUtil;

public class OpDefinitionPage extends PageUtil {

    public OpDefinitionPage(WebDriver webDriver){
        super(webDriver);
    }

    private final By popupButton = By.cssSelector("#okButton [role]");
    private final By topbarUser = By.cssSelector(".kt-margin-0.kt-margin-r-5");
    private final By newButton = By.cssSelector("#newButton [role]");
    private final By codeInput = By.cssSelector("#code .dx-texteditor-input");
    private final By polyvalenceCombo = By.cssSelector("[aria-haspopup='listbox']");
    private final By polyvalenceComboItem = By.cssSelector("div:nth-of-type(2) > .dx-item-content.dx-list-item-content");
    private final By nameSurnameInput = By.cssSelector("#name .dx-texteditor-input");
    private final By passwordInput = By.cssSelector("#password .dx-texteditor-input");
    private final By startDateIcon = By.cssSelector("#startDate .dx-dropdowneditor-icon");
    private final By todayButton = By.cssSelector(".dx-toolbar-before > .dx-item.dx-toolbar-button.dx-toolbar-item > .dx-item-content.dx-toolbar-item-content > div[role='button']");
    private final By okButton = By.cssSelector(".dx-datebox-wrapper.dx-datebox-wrapper-calendar.dx-datebox-wrapper-datetime.dx-popup-cancel-visible.dx-popup-done-visible.dx-popup-wrapper  div[role='toolbar']  .dx-toolbar-after > div:nth-of-type(1) > .dx-item-content.dx-toolbar-item-content > div[role='button']");
    private final By saveOpButton = By.cssSelector("#saveButton > dx-button");
    private final By areUSureButton = By.xpath("//button[contains(text(),'Yes')]");
    private final By successDiv = By.cssSelector("div#swal2-content");
    private final By successDivClose = By.xpath("//button[contains(text(),'OK')]");
    private final By opCodeFilter = By.cssSelector("td:nth-of-type(2) input[role='textbox']");
    private final By opCodeSectionInFilter = By.cssSelector("div:nth-of-type(6) table[role='presentation']  tr[role='row'] > td:nth-of-type(2)");

    @Step("Verify that welcome message is 'Hi username' at the top right of the screen...")
    public void verifyLoginUser(String username) {
        // POPUP AÇILMASI İÇİN 2 SANİYE BEKLENİYOR.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clickTo(popupButton);
        Assert.assertEquals(
                getTextTo(topbarUser),
                "Hi " + username,
                "Kullanıcı girişi hatası!");
    }

    @Step("Press 'New' Button to add new Operator item...")
    public void addNewOp() {
        clickTo(newButton);
    }

    @Step("Enter the information and press 'Save' button...")
    public void addNewOpInfo(String opCode, String polyvalence, String opUser, String opPass) {
        sendKeysTo(codeInput, opCode);
        // TODO polyvalence DEĞERİNE GÖRE SEÇİM YAPILACAK...
        //Select select = new Select(polyvalenceCombo);
        //select.selectByIndex(Integer.parseInt(polyvalence));
        clickTo(polyvalenceCombo);
        clickTo(polyvalenceComboItem);
        sendKeysTo(nameSurnameInput, opUser);
        sendKeysTo(passwordInput, opPass);
        clickTo(startDateIcon);
        clickTo(todayButton);
        clickTo(okButton);
        clickTo(saveOpButton);
    }

    @Step("Press 'Yes' button to save the data...")
    public void confirmNewOpCode() {
        clickTo(areUSureButton);
    }

    @Step("Verify that success message contains 'Was recorded!' then press 'OK' button...")
    public void verifyAddingNewOperatorMessage() {
        // BİR ÖNCEKİ POPUP KAPANIP YENİSİ AÇILANA KADAR 2 SANİYE BEKLENİYOR.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(
                getTextTo(successDiv),
                "The registration process has been successfully realized.",
                "Operatör ekleme hatası!");
        clickTo(successDivClose);
    }

    @Step("Filter by 'Code' column with operator code and verify that...")
    public void verifyNewOperatorCode(String opCode) {
        sendKeysTo(opCodeFilter, opCode);
        // FİLTRENİN ÇALIŞMASI İÇİN 2 SANİYE BEKLENİYOR.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(
                getTextTo(opCodeSectionInFilter),
                opCode,
                "Eklenen operatör bulunamadı!");
    }
}
