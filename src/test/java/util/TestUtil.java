package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import page.LoginPage;
import page.OpDefinitionPage;

public abstract class TestUtil {

    String chromedriver = DataManager.getInstance().getChromedriver();
    String geckodriver = DataManager.getInstance().getGeckodriver();
    String baseurl = DataManager.getInstance().getBaseUrl();
    String username = DataManager.getInstance().getUsername();
    String password = DataManager.getInstance().getPassword();

    public WebDriver webDriver;
    protected LoginPage loginPage;
    protected OpDefinitionPage opDefinitionPage;

    @BeforeClass
    @Parameters({"browser"})
    public void setupDriver(@Optional("Chrome") String browser) {

        if (browser.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", chromedriver);
            webDriver = new ChromeDriver();
            webDriver.manage().window().maximize();

        } else if (browser.equalsIgnoreCase("firefox")) {
            System.setProperty("webdriver.gecko.driver", geckodriver);
            webDriver = new FirefoxDriver();
            webDriver.manage().window().maximize();

        } else {
            System.out.println(browser + " tarayıcı desteklenmiyor!");
            System.exit(1);
        }
    }

    @BeforeMethod(alwaysRun = true)
    public void loginApp() {
        loginPage = new LoginPage(webDriver);
        loginPage.open(baseurl);
        loginPage.login(username, password);
        opDefinitionPage = new OpDefinitionPage(webDriver);
        opDefinitionPage.verifyLoginUser(username);
    }

    @AfterMethod(alwaysRun = true)
    public void logoutApp() {
        loginPage.logout();
    }

    @AfterMethod
    public void deleteNewOperator() {
        // EKLENEN OPERATOR VERİ TABANINDAN SİLİNEBİLİR.
    }

    @AfterClass
    public void quitDriver() {
        webDriver.quit();
    }
}
