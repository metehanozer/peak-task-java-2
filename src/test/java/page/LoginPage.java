package page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import util.PageUtil;

public class LoginPage extends PageUtil {

    public LoginPage(WebDriver webDriver){
        super(webDriver);
    }

    private final By usernameLabel = By.cssSelector("#userNamePlaceHolder");
    private final By passwordLabel = By.cssSelector("#passwordPlaceHolder");
    private final By loginLabel = By.cssSelector("#loginLabel");
    private final By topBarUser = By.cssSelector(".kt-header__topbar-user");
    private final By signOutButton = By.cssSelector("user-account-management pm-button > dx-button[role='button'] > .dx-button-content.dx-template-wrapper");

    public void open(String baseurl) {
        navigateTo(baseurl);
        // LOGIN SAYFASINDA OTOMATİK GİRİŞ BİLGİLERİ GELDİĞİ İÇİN BURAYA SLEEP KOYMAK ZORUNDA KALDIM.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Step("Login the application by using credentials...")
    public void login(String username, String password) {
        clearSendKeysTo(usernameLabel, username);
        clearSendKeysTo(passwordLabel, password);
        clickTo(loginLabel);
    }

    @Step("Logout the application...")
    public void logout() {
        clickTo(topBarUser);
        clickTo(signOutButton);
    }
}
