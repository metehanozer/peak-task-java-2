# 🎁 peak-task-java-2
Java Selenium webDriver test project with page object model

### Test

chrome test
~~~
mvn clean test
~~~

firefox test
~~~
mvn clean test -Dbrowser=firefox
~~~

chrome & firefox parallel test
~~~
mvn clean test -Dsuite=parallel
~~~

allure report
~~~
mvn allure:serve
~~~