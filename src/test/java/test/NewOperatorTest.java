package test;

import java.io.FileReader;
import java.util.Random;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import util.TestUtil;

@Epic("Regression Tests")
@Feature("Add Operator Tests")
public class NewOperatorTest extends TestUtil {

    @DataProvider(name = "operatorList")
    public Object[][] getOperatorList() {

        String jsonFilePath = "operatorList.json";
        Object[][] returnData = null;

        try (FileReader reader = new FileReader(ClassLoader.getSystemResource(jsonFilePath).getFile())) {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            JSONArray jsonArray = (JSONArray) jsonObject.get("operatorList");

            returnData = new Object[jsonArray.size()][4];
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jObject = (JSONObject) jsonArray.get(i);
                returnData[i][0] = jObject.get("opCode");
                returnData[i][1] = jObject.get("polyvalence");
                returnData[i][2] = jObject.get("opUser");
                returnData[i][3] = jObject.get("opPass");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return returnData;
    }

    @Test(dataProvider = "operatorList", description = "Adding new operator...")
    public void addingNewOperator(String opCode, String polyvalence, String opUser, String opPass) throws InterruptedException {

        // OPERATÖR BİLGİLERİNE RASTGELE SAYI EKLENİYOR.
        Random random = new Random();
        int randomNumber = random.nextInt(1000);
        opCode = opCode + randomNumber;
        opUser = opUser + randomNumber;
        opPass = opPass + randomNumber;

        System.out.println("checkNewOperator testi başladı. " + opCode);

        opDefinitionPage.addNewOp();

        opDefinitionPage.addNewOpInfo(opCode, polyvalence, opUser, opPass);

        opDefinitionPage.confirmNewOpCode();

        opDefinitionPage.verifyAddingNewOperatorMessage();

        opDefinitionPage.verifyNewOperatorCode(opCode);
    }
}
