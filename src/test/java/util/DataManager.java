package util;

import java.io.IOException;
import java.util.Properties;

public class DataManager {

    private static DataManager instance;
    private static final Object lock = new Object();
    private static String chromedriver;
    private static String geckodriver;
    private static String baseUrl;
    private static String username;
    private static String password;

    public static DataManager getInstance () {
        if (instance == null) {
            synchronized (lock) {
                instance = new DataManager();
                instance.loadData();
            }
        }
        return instance;
    }

    private void loadData() {
        Properties properties = new Properties();

        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream("Test.properties"));
        } catch (IOException e) {
            System.out.println("test properties file cannot be found");
        }

        chromedriver = properties.getProperty("chromedriver");
        geckodriver = properties.getProperty("geckodriver");
        baseUrl = properties.getProperty("baseurl");
        username = properties.getProperty("username");
        password = properties.getProperty("password");
    }

    public String getChromedriver() {
        return chromedriver;
    }

    public String getGeckodriver() {
        return geckodriver;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
